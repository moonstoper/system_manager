import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:battery_info/battery_info_plugin.dart';
import 'package:battery_info/enums/charging_status.dart';
import 'package:battery_info/model/android_battery_info.dart';
import 'package:battery_info/model/iso_battery_info.dart';
import 'package:battery/battery.dart';

class Batterywidget extends StatefulWidget {
  const Batterywidget({
    Key key,
  }) : super(key: key);

  @override
  _BatterywidgetState createState() => _BatterywidgetState();
}

class _BatterywidgetState extends State<Batterywidget> {
  final logger = new Logger();
   BatteryInfoPlugin vars = new BatteryInfoPlugin();
  int valueBattery = 0;
  @override
  void initState() {
    super.initState();
    batteryDetails();
  }

  Future<void> batteryDetails() async {
   
    AndroidBatteryInfo v = await vars.androidBatteryInfo;

    // valueBattery = await Battery().batteryLevel;
    logger.d(v.chargingStatus == ChargingStatus.Charging);
    // logger.d(valueBattery);
    // return;
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 300,
      left: 30,
      child: Container(
          height: 100,
          width: 130,
          child: Stack(
            children: <Widget>[
              Positioned(
                  child: Text(
                "Battery",
                style: TextStyle(
                  fontFamily: "Poppins",
                  fontSize: 35,
                ),
              )),
              Positioned(
                child: Text(
                  "Health:" + valueBattery.toString(),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                top: 40,
              )
            ],
          )),
    );
  }
}
