import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'dart:async';
import 'package:system_info/system_info.dart';
import 'dart:io';

import 'package:systemapp/pages/drawer.dart';

var logger = new Logger();

class SocnCPU extends StatefulWidget {
  SocnCPU({Key key}) : super(key: key);

  @override
  _SocnCPUState createState() => _SocnCPUState();
}

class _SocnCPUState extends State<SocnCPU> {
  @override
  void initState() {
    super.initState();
    logger.d("${SysInfo.kernelArchitecture}");
    //getInfo();
  }

  Future<void> getInfo() async {
    await Process.run("cat", ["/proc/cpuinfo"]).then((ProcessResult value) {
      logger.d(value.stdout);
    }).catchError((onError) => {logger.e(onError)});
    // await Process.run("ls", ["-l"]).then((ProcessResult value) {
    //   var v = value.stdout;
    //   logger.d(v);
    // });

    return;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
            top: 200,
            left: 240,
            child: Text(
              "${SysInfo.processors.first.vendor}\n ${SysInfo.processors.first.architecture}",
              style: TextStyle(
                  fontSize: 20,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.bold),
            ))
      ],
    );
  }
}
