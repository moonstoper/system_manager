import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';
import 'package:systemapp/pages/battery_dashboard.dart';
import 'package:systemapp/pages/drawer.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'dart:async';
import 'package:logger/logger.dart';
import 'package:intl/intl.dart';
import 'package:systemapp/pages/socncpu.dart';

var logger = new Logger();

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);
  static const String routeName = "/home";
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //GlobalKey globalKeyA = GlobalKey(debugLabel: 'A');

  final GlobalKey<ScaffoldState> _scafoldhomekey =
      new GlobalKey<ScaffoldState>();
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  String brand;
  String formatDates;
  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;

    deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);

    if (!mounted) return;

    setState(() {
      log("data set");
      _deviceData = deviceData;
      //logger.d(_deviceData["version.securityPatch"]);
      var tags = _deviceData["version.securityPatch"].toString();
      var splits = tags.split("-");
      logger.d(splits);
      DateTime xyz = DateTime(int.parse(splits[0]), int.parse(splits[1]));
      DateFormat formatter = DateFormat("MMM y");
      formatDates = formatter.format(xyz);
      logger.d(formatDates);
      brand = deviceData["brand"];
    });
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 40,
      extendBody: true,
      key: _scafoldhomekey,
      drawer: MyDrawer(
        brandvalue: _deviceData["brand"],
        modelvalue: _deviceData["model"],
      ),
      body: Stack(
        clipBehavior: Clip.none,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(vertical: 60, horizontal: 15),
            child: IconButton(
                iconSize: 30,
                icon: Icon(Icons.menu),
                onPressed: () => {
                      _scafoldhomekey.currentState.openDrawer(),
                    }),
          ),
          Container(
            width: 200,
            height: 50,
            // color: Colors.red,
            margin: EdgeInsets.symmetric(vertical: 60, horizontal: 50),
            alignment: Alignment.center,
            child: Text(
              "System Health",
              style: TextStyle(fontSize: 25, fontFamily: "Poppins"),
            ),
            //decoration: BoxDecoration(color: Colors.blue),
          ),
          basicAndriodDetails(),
          rootBox(),
          //Batterywidget(),  put on  hold for now
          SocnCPU(),
        ],
      ),
    );
  }

  Color themChange(String type) {
    if (type == "font") {
      return Theme.of(context).accentColor == ThemeData.dark().accentColor
          ? Colors.white
          : Colors.black;
    } else {
      return Theme.of(context).accentColor == ThemeData.dark().accentColor
          ? Colors.deepOrangeAccent
          : Colors.white;
    }
  }

  Positioned rootBox() {
    return Positioned(
        top: 270,
        left: 10,
        child: Container(
          height: 50,
          width: 200,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              rootcheck()
                  ? "Device :\t" + "Not Rooted"
                  : "Device :\t" + "Rooted",
              style: TextStyle(
                  fontSize: 20,
                  fontFamily: "Poppins",
                  color: rootcheck() ? Colors.green : Colors.orange),
            ),
          ),
        ));
  }

  bool rootcheck() {
    return check1() || check2() || check3();
  }

  bool check1() {
    return _deviceData["tags"].toString() == "release-keys";
  }

  bool check2() {
    //paths to check for root
    Set<String> paths = {
      "/system/app/Superuser.apk",
      "/sbin/su",
      "/system/bin/su",
      "/system/xbin/su",
      "data/local/xbin/su",
      "/system/bin/failsafe/su"
    };
    for (String path in paths) {
      if (new File(path).existsSync()) return true;
    }
    return false;
  }

  bool check3() {
    bool rooted = false;
    Process.run("which", ["su"])
        .then((value) => {
              if (value != null) {rooted = true},
              logger.d(value)
            })
        .catchError((error) => {logger.e(error)});

    return rooted;
  }

  //Andriod Version and Model
  Container basicAndriodDetails() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 150, horizontal: 20),
      child: SizedBox(
          child: Container(
        width: 200,
        height: 120,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 30),
        child: Stack(
          children: <Widget>[
            Container(
              child: Text(
                _deviceData["version.release"] != null
                    ? "Andriod " + _deviceData["version.release"].toString()
                    : "Andriod",
                style: TextStyle(
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.bold,
                  fontSize: 45,
                ),
              ),
            ),
            Positioned(
              width: 140,

              left: 180,
              //padding: EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                _deviceData["supported64BitAbis"].toString() != null
                    ? "64"
                    : "32",
                style: TextStyle(
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Positioned(
              height: 100,
              top: 50,
              child: Text(
                _deviceData["version.securityPatch"] != null
                    ? "Security Patch \n" + formatDates
                    : "",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Positioned(
                height: 100,
                top: 50,
                left: 140,
                child: Text(
                  "xiaomi",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ))
          ],
        ),
      )),
    );
  }
}
