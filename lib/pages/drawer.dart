import 'dart:convert';
import 'dart:developer';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:device_apps/device_apps.dart';
import 'package:logger/logger.dart';

var logger = Logger();

class MyDrawer extends StatelessWidget {
  final String brandvalue;
  final String modelvalue;
  const MyDrawer({Key key, this.brandvalue, this.modelvalue}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // logger.d(Theme.of(context).accentColor);
    // logger.d(ThemeData.dark().accentColor);

    return Drawer(
      child: ListView(
        padding: const EdgeInsets.all(0),
        children: <Widget>[
          headerDrawer(),
          ListTile(
            leading: Icon(Icons.settings_applications),
            title: Text("System Settings"),
            onTap: () {
              log("clicked");
              ontappy();
              log("message");
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text("Settings"),
            hoverColor: Colors.redAccent,
            focusColor: Colors.red,
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text("About"),
            hoverColor: Colors.redAccent,
            focusColor: Colors.red,
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Future<void> ontappy() async {
    // List<Application> appsv = await DeviceApps.getInstalledApplications(
    //     includeSystemApps: true, onlyAppsWithLaunchIntent: true);
    // //print(appsv);
    // int n = 0;
    // while (appsv[n] != null) {
    //   if (appsv[n].packageName == "com.android.settings") {
    //     // logger.d(appsv[n]);
    //     break;
    //   }
    //   n += 1;
    // }
    // //logger.d(JsonEncoder(appsv));
    // logger.e("com.android.settings" == appsv[n].packageName);
    // logger.e(appsv[n].packageName);
    await DeviceApps.openApp("com.android.settings");
  }

  UserAccountsDrawerHeader headerDrawer() {
    return UserAccountsDrawerHeader(
        accountName: Text(this.brandvalue != null ? this.brandvalue : "Hello",
            style: TextStyle(
                fontSize: 35, fontFamily: "Poppins", color: Colors.white)),
        accountEmail: Text(this.modelvalue != null ? this.modelvalue : ""),
        decoration: BoxDecoration(
          color: Colors.black87,
        ));
  }
}
